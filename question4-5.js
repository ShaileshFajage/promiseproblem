
const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]




function signIn(userName) {

    return new Promise((resolve, reject)=>{

        let date = new Date();

        let seconds = date.getSeconds();
        console.log(seconds);

        if(seconds>30)
        {
            resolve(`Congratulations ${userName} : Sign In success : code 200 `);
        }
        else
        {
            reject(`Sorry ${userName} : Sign Failed : code 404`);
        }
    })
}



function getBooks(x, Books) {
    
    return new Promise((resolve, reject)=>{

        if(x==1)
        {
            // console.log('GetBooks - Success');
            let res = 'GetBooks - Success'
            let booksName =  Books.reduce((res, each)=>{
                res.push(each.name)
                return res;
            },[])

            resolve(res, booksName);
        }
        else
        {
            reject("GetBooks - Failure")
        }
    })
}

var activity = [];

function logData() {
    
    return new Promise((resolve, reject)=>{

        setTimeout(() => {

            signIn("abc").then((res)=>{

                // console.log(res);
    
                activity.push(res)
            
                getBooks(0,Books).then((res, bookDetails)=>{
            
                    //console.log(bookDetails);
                    // console.log('Hi');
                    activity.push(res)
                    resolve(activity)
                    
                })
                .catch((err)=>{
                    // console.log(err);
                    activity.push(err)
                    resolve(activity)
                })
                
            })
            .catch((err)=>{
                // console.log(err);
                activity.push(err)
                resolve(activity)
            })
            
        }, 1000);
        
    })
}


function displayActivities() {
    
    logData().then((activities)=>{
        console.log(activities);
    })
}

displayActivities();