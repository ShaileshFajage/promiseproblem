const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]



function getBooks(x, Books) {
    
    return new Promise((resolve, reject)=>{

        if(x==1)
        {
            let booksName =  Books.reduce((res, each)=>{
                res.push(each.name)
                return res;
            },[])


            resolve(booksName);
        }
        else
        {
            reject("Error occured : code 404")
        }
    })
}

getBooks(1, Books).then((res)=>{
    console.log('Books fetched successfully : code 200');
    console.log(res);
})
.catch((err)=>{
    reject("Books fetching failed : code 404")
    console.log(err);
})