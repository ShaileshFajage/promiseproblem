
const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]

function signIn(userName) {

    return new Promise((resolve, reject)=>{

        let date = new Date();

        let seconds = date.getSeconds();
        console.log(seconds);

        if(seconds>30)
        {
            resolve(`Congratulations ${userName} : Sign In success : code 200`);
        }
        else
        {
            reject(`Sorry ${userName} : Sign Failed : code 404`);
        }
    })
}



function getBooks(x, Books) {
    
    return new Promise((resolve, reject)=>{

        if(x==1)
        {
            console.log('GetBooks - Success');
            let booksName =  Books.reduce((res, each)=>{
                res.push(each.name)
                return res;
            },[])

            resolve(booksName);
        }
        else
        {
            reject("GetBooks - Failure")
        }
    })
}



signIn("Mary").then((res)=>{
    console.log(res);

    getBooks(1,Books).then((res)=>{

        console.log(res);
    })
    .catch((err)=>{
        console.log(err);
    })
    
})
.catch((err)=>{
    console.log(err);
})


signIn("Emily").then((res)=>{
    console.log(res);

    getBooks(1,Books).then((res)=>{

        console.log(res);
    })
    .catch((err)=>{
        console.log(err);
    })
    
})
.catch((err)=>{
    console.log(err);
})