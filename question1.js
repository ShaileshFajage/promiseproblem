
function signIn(userName) {

    return new Promise((resolve, reject)=>{

        let date = new Date();

        let seconds = date.getSeconds();
        console.log(seconds);

        if(seconds>30)
        {
            resolve(`Congratulations ${userName} : Sign In success : code 200   `);
        }
        else
        {
            reject(`Sorry ${userName} : Sign Failed : code 404`);
        }
    })
}

signIn("abc").then((res)=>{
    console.log(res);
})
.catch((err)=>{
    console.log(err);
})