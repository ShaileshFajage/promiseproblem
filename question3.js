
const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]

function signIn(userName) {

    return new Promise((resolve, reject)=>{

        let date = new Date();

        let seconds = date.getSeconds();
        console.log(seconds);

        if(seconds>30)
        {
            resolve(`Congratulations ${userName} : Sign In success : code 200 `);
        }
        else
        {
            reject(`Congratulations ${userName} : Sign In success : code 200 `);
        }
    })
}



function getBooks(x, Books) {
    
    return new Promise((resolve, reject)=>{

        if(x==1)
        {
            console.log('GetBooks - Success');
            let booksName =  Books.reduce((res, each)=>{
                res.push(each.name)
                return res;
            },[])

            resolve(booksName);
        }
        else
        {
            reject("Books fetching failed : code 404")
        }
    })
}



signIn("abc").then((res)=>{
    console.log(res);

    getBooks(1,Books).then((res)=>{

        console.log('Books fetched successfully : code 200');

        console.log(res);
    })
    .catch((err)=>{
        console.log(err);
    })
    
})
.catch((err)=>{
    console.log(err);
})

